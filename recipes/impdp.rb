#
# Cookbook Name:: oracle
# Recipe:: createdb
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
## Create Oracle databases.
#


# Iterate over :oracle[:rdbms][:dbs]'s keys, Initializing dbca to
# create a database named after the key for each key whose associated
# value is false, and flipping the value afterwards.
# If :oracle[:rdbms][:dbs] is empty, we print a warning to STDOUT.

ruby_block "print_empty_impdp_hash_warning" do
  block do
    Chef::Log.warn(":oracle[:rdbms][:impdp] is empty; no dumpfile import to be executed.")
  end
  action :create
  only_if {node[:oracle][:rdbms][:impdp].empty?}
end

puts JSON.pretty_generate(node[:oracle])


node[:oracle][:rdbms][:impdp].each_key do |impdp|


  # if node[:oracle][:rdbms][:impdp][impdp][:completed]
  #   ruby_block "print_#{impdp}_skipped_msg" do
  #     block do
  #       Chef::Log.info("Import script #{impdp} has already been applied to the database - skipping it.")
  #     end
  #     action :create
  #   end
  #
  #   next
  # end


  template "#{node[:oracle][:rdbms][:install_dir]}/#{impdp}" do
    source 'import.par.erb'
    owner 'oracle'
    cookbook node[:oracle][:rdbms][:impdp][impdp][:cookbook]
    group 'oinstall'
    mode '0644'
    variables ({'impdp' => node[:oracle][:rdbms][:impdp][impdp]})
  end

  node[:oracle][:rdbms][:impdp][impdp][:dumpfile_urls].each do |dumpfile_url|
    uri = URI.parse(dumpfile_url)
    dumpfile_name = File.basename(uri.path)

    execute "fetch_dump_file_#{dumpfile_url}" do
      command "curl #{node[:oracle][:curl_options]} #{dumpfile_url}"
      user "oracle"
      group 'oinstall'
      cwd node[:oracle][:rdbms][:impdp][impdp][:dpdump_location]
      not_if {::File.exist?("#{node[:oracle][:rdbms][:impdp][impdp][:dpdump_location]}/#{dumpfile_name}")}
    end
  end


  bash "impdp_execute_#{impdp}" do
    user "oracle"
    group "oinstall"
    environment (node[:oracle][:rdbms][:env])
    code "impdp ignore=y parfile=#{node[:oracle][:rdbms][:install_dir]}/#{impdp}"
    returns [0, 5] #ignore errorlevel 5 "objects already exist"
  end


end
