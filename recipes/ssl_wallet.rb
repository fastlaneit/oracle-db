#
# Cookbook Name:: oracle
# Recipe:: ssl_wallet
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
## Reconfigure Oracle instance with SSL
#
#
#

uri = URI.parse(node[:oracle][:rdbms][:ssl_wallet][:keystore_url])
keystore_filename = File.basename(uri.path)

execute "fetch_keystore_file_#{keystore_filename}" do
  command "curl #{node[:oracle][:curl_options]} #{node[:oracle][:rdbms][:ssl_wallet][:keystore_url]}"
  user "oracle"
  group 'oinstall'
  cwd '/tmp'
  not_if {::File.exist?("/tmp/#{keystore_filename}")}
end



template "#{node[:oracle][:rdbms][:ora_home]}/network/admin/listener.ora" do
  source 'listener-secure.ora.erb'
  cookbook node[:oracle][:rdbms][:ssl_wallet][:cookbook]
  owner 'oracle'
  group 'oinstall'
  mode '0644'
end


template "#{node[:oracle][:rdbms][:ora_home]}/network/admin/tnsnames.ora" do
  source 'tnsnames-secure.ora.erb'
  cookbook node[:oracle][:rdbms][:ssl_wallet][:cookbook]
  owner 'oracle'
  group 'oinstall'
  mode '0644'
end

template "#{node[:oracle][:rdbms][:ora_home]}/network/admin/sqlnet.ora" do
  source 'sqlnet-secure.ora.erb'
  cookbook node[:oracle][:rdbms][:ssl_wallet][:cookbook]
  owner 'oracle'
  group 'oinstall'
  mode '0644'
end


script 'create_wallet' do
  interpreter "bash"
  cwd "#{node[:oracle][:ora_base]}"
  user 'oracle'
  group 'oinstall'
  environment (node[:oracle][:rdbms][:env])
  code <<-EOH
    mkdir -p #{node[:oracle][:ora_base]}/wallet
    cd #{node[:oracle][:ora_base]}/wallet
    orapki wallet create -wallet ./server_wallet -auto_login -pwd "#{node[:oracle][:rdbms][:ssl_wallet][:wallet_password]}" 
    orapki wallet jks_to_pkcs12 -wallet ./server_wallet -pwd "#{node[:oracle][:rdbms][:ssl_wallet][:wallet_password]}" -keystore /tmp/#{keystore_filename} -jkspwd "#{node[:oracle][:rdbms][:ssl_wallet][:keystore_password]}"
    lsnrctl stop
    lsnrctl start
  EOH
  #not_if { ::File.exist?("#{node[:oracle][:ora_base]}/wallet") }
end

