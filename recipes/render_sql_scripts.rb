#
# Cookbook Name:: oracle
# Recipe:: createdb
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
## Create Oracle databases.
#


# Prepare any init SQL scripts as declared in attributes\

node[:oracle][:rdbms][:dbs].each_key do |db|

  ruby_block "print_empty_db_init_hash_warning" do
    block do
      Chef::Log.warn(":oracle[:rdbms][:initscripts] is empty; no scripts to be executed.")
    end
    action :create
    only_if {node[:oracle][:rdbms][:dbs][db][:initscripts].empty?}
  end

  node[:oracle][:rdbms][:dbs][db][:initscripts].each_key do |initscript|

    if node[:oracle][:rdbms][:dbs][db][:initscripts][initscript]
      ruby_block "print_#{initscript}_skipped_#{db}_msg" do
        block do
          Chef::Log.info("Script #{initscript} has already been applied to the database #{db} - skipping it.")
        end
        action :create
      end

      next
    end

    template "#{node[:oracle][:rdbms][:install_dir]}/#{initscript}" do
      owner 'oracle'
      group 'oinstall'
      cookbook lazy {node[:oracle][:rdbms][:dbs][db][:initscripts_cookbook]}
      variables ({'db' => db, 'dbdata' => "#{node[:oracle][:rdbms][:dbs_root]}/#{db}"})

      mode '0644'
    end

  end

end
