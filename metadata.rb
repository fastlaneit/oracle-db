name             'oracle-db'
maintainer       'Fastlane Solutions'
maintainer_email 'hello@fastlane-it.com'
license          'Apache 2.0'
description      'Installs/Configures Oracle rdbms and client on CentOS 7'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.2.4'
supports         'redhat'
supports         'centos'
