#
#

resource_name :sql_script
provides :sql_script
default_action :nothing

property :source, String, required: true
property :oracle_auth, String, required: true
property :environment, Hash, required: false

property :user, String, required: false
property :group, String, required: false


load_current_value do |desired|

end

action :run do
  run_sql_script
end


action_class do

  def run_sql_script
    #return if user_account_disabled? || credential_up_to_date?

    converge_by('Creating file /tmp/myfile') do
      ret = shell_out!("sqlplus  #{new_resource.oracle_auth} < #{new_resource.source}",
                       environment: @new_resource.environment,
                       user: @new_resource.user,
                       group: @new_resource.group)
      puts ">>>>>>>> #{ret.stdout}"
    end
  end

end
